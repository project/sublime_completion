<?php

/**
 * @file
 *   sublime module drush integration.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @return array An associative array describing the command.
 */
function sublime_drush_command() {
  $items = array();
  $items['sublime-create'] = array(
    'description' => "Create a new sublime text autocomplete file from a drupal docroot.",
    'aliases' => array('sc', 'sublime'),
    'arguments' => array(
      'path' => 'Optional. The custom directory path to save the completions file in.',
    ),
    'examples' => array(
      'drush sublime' => 'Scan through the current project and build a completions file in the default location.',
      'drush sublime /path/to/directory' => 'Scan through the current project and build a completions file in the specified location. The file will be named Drupal.sublime-completions.',
      'drush sublime /path/to/directory/mycustomname.sublime-completions' => 'Scan through the current project and build a completions file in the specified location with the specified custom file name.',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * @param $section
 *
 * @return mixed
 */
function sublime_drush_help($section) {
  switch ($section) {
    case 'drush:sublime-create':
      return dt("Creates (if new) or overwrites your Sublime Text 2 Drupal-sublime-completions file.

This file gives Sublime Text 2 the ability to autocomplete Drupal core and module functions with tab stop paramaters, much like the built in PHP autocompletions. It should be run anywhere within a Drupal document root and will scan the whole project structure including core files, all contrib modules as well as custom modules.

Scanning is fairly swift so its safe to run whenever new modules are added to the project. Note that this completions file is system wide so if you create it for a Drupal 6 project and then switch to a Drupal 7 project you will want to regenerate the completions file.");
  }
}

/**
 * Get a list of all feature modules.
 *
 * @param string $path
 */
function drush_sublime_create($path = NULL) {
  // File types to search in
  $extensions = array(
    'php',
    'inc',
    'module',
  );
  // Get the docroot
  $root = _drush_core_directory();
  // Scan through each file in the project and build an array containing the functions and parameters.
  $di = new RecursiveDirectoryIterator($root);
  foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
    if (in_array(end(explode('.', $filename)), $extensions)) {
      // Open the file
      $contents = file_get_contents($filename);
      // Retrieve functions from file.
      preg_match_all('/function\s(.+?)\((?:(.*?))?\)\s{$/im', $contents, $matches);
      foreach ($matches[1] as $key => $function_name) {
        //{ "trigger": "actions_do", "contents": "actions_do(${1:action_ids}, ${2:&object}, ${3:context = NULL}, ${4:a1 = NULL}, ${5:a2 = NULL})" },
        $paramstring = str_replace(', ', ',', trim($matches[2][$key]));
        $params = explode(',', str_replace('"', '\"', $paramstring));
        $i = 1;
        $function_params = array();
        // var_dump($params);
        if ($params[0] != '') {
          foreach ($params as $param) {
            $function_params[] = '${' . $i . ':' . str_replace('$', '', $param) . '}';
            $i++;
          }
        }
        $lines[$function_name] = '    { "trigger": "' . $function_name . '", "contents": "' . $function_name . '(' . implode(', ', $function_params) . ')" }';
      }
    }
  }

  // Create the string containing all the $lines in the right format.
  $content = '{
  "scope": "source.php - variable.other.php",

  "completions":
  [
    "php",
' . "\n";
  $content .= implode(",\n", $lines);
  $content .= "
  ]
}";

  // Determine whether a custom path has been provided as a parameter.
  if ($path != NULL) {
    if (!strpos($path, 'sublime-completions')) {
      $fullpath = substr($path, -1) !== '/' ?
        $path . '/Drupal.sublime-completions' : $path . 'Drupal.sublime-completions';
    }
    else {
      $fullpath = $path;
    }
  }
  else {
    // Check if completions file exists in Mac or Linux locations.
    $syspath = is_dir('~/.config/sublime-text-2') ?
      '/.config/sublime-text-2/' : '/Library/Application Support/Sublime Text 2/';
    $fullpath = getenv('HOME') . $syspath . 'Packages/User/Drupal.sublime-completions';
  }

  // Write the string to the file replacing anything already there.
  if (file_put_contents($fullpath, $content)) {
    $row = dt('The completions file was successfully created at !path', array('!path' => $fullpath));
  } else {
    if ($path != NULL) {
      $row = dt('There was a problem creating the completions file in your custom path. Please ensure the path is correct.');
    }
    else {
      $row = dt('There was a problem creating the completions file.');
    }
  }
  drush_print($row);
}